package com.stevetuto.batch.batch;

import com.stevetuto.batch.model.User;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
@Qualifier("userProcessor")
public class Processor implements ItemProcessor<User, User> {

    private static final Map<String, String> DEPT_NAMES= new HashMap<>();

    public Processor(){
        DEPT_NAMES.put("001", "Technology");
        DEPT_NAMES.put("002","Operations");
        DEPT_NAMES.put("003", "Accounts");
    }

    @Override
    public User process(User user) throws Exception {
        String deptCode = user.getDept();
        String dept = DEPT_NAMES.get(deptCode);
        user.setDept(dept);
        user.setTime(new Date());
        System.out.println(String.format("Converted from [%s] to [%s]", deptCode, dept));
        return user;
    }
}
