package com.stevetuto.batch.batch;

import com.stevetuto.batch.model.Person;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;


@Component
@Qualifier("personProcessor")
public class PersonItemProcessor implements ItemProcessor<Person, Person>{

    @Override
    public Person process(Person person) throws Exception {
        return person;
    }
}
