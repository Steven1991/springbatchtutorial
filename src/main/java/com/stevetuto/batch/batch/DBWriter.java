package com.stevetuto.batch.batch;

import com.stevetuto.batch.model.User;
import com.stevetuto.batch.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.batch.item.ItemWriter;
import java.util.List;

@Component
public class DBWriter implements ItemWriter<User> {

    @Autowired
    UserRepository userRepository;

    @Override
    public void write(List<? extends User> users) throws Exception{

        System.out.println("Data Saved for Users: " +users);

        userRepository.save(users);
    }



}
