package com.stevetuto.batch.config;

import com.stevetuto.batch.batch.PersonItemProcessor;
import com.stevetuto.batch.model.Person;
import com.stevetuto.batch.repository.PersonRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;


public class BatchConf {
    /***************from DB to csv file ************************/

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    @Qualifier("userProcessor")
    private ItemProcessor userProcessor;

    @Autowired
    @Qualifier("personProcessor")
    private  ItemProcessor personProcessor;

    @Autowired
    public DataSource dataSource;

    @Bean
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/filedb");
        dataSource.setUsername("root");
        dataSource.setPassword("");

        return dataSource;
    }

    @Bean
    public JdbcCursorItemReader<Person> reader(){
        // List<Person> reader = new ArrayList<>();
        JdbcCursorItemReader<Person> reader = new JdbcCursorItemReader<Person>();
        reader.setDataSource(dataSource());
        reader.setSql("SELECT id,name FROM person");
        reader.setRowMapper(new PersonRowMapper());

        //reader =personRepository.findAll();

        return reader;
    }



    @Bean
    public PersonItemProcessor personProcessor(){
        return new PersonItemProcessor();
    }

    @Bean
    public FlatFileItemWriter<Person> writer(){
        FlatFileItemWriter<Person> writer = new FlatFileItemWriter<Person>();
        writer.setResource(new ClassPathResource("persons.csv"));
        writer.setLineAggregator(new DelimitedLineAggregator<Person>() {{
            setDelimiter(",");
            setFieldExtractor(new BeanWrapperFieldExtractor<Person>() {{
                setNames(new String[] { "id", "name" });
            }});
        }});

        return writer;
    }



    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1").<Person, Person> chunk(10)
                .reader((ItemReader<? extends Person>) reader())
                .processor(personProcessor())
                .writer(writer())
                .build();
    }

    @Bean
    public Job exportUserJob() {
        return jobBuilderFactory.get("exportUserJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1())
                .end()
                .build();
    }
}
