package com.stevetuto.batch.config;

import com.stevetuto.batch.model.Person;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonRowMapper implements RowMapper<Person> {

    @Override
    public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
        Person user = new Person();
        user.setId(rs.getInt("id"));
        user.setName(rs.getString("name"));

        return user;
    }

}