package com.stevetuto.batch.repository;

import com.stevetuto.batch.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository  extends JpaRepository<Person, Integer> {
}
